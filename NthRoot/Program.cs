﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Entrez le nombre (number) : ");
double number = Convert.ToDouble(Console.ReadLine());


Console.WriteLine("Entrez l'indice de la racine (root) : ");
int root = Convert.ToInt32(Console.ReadLine());

Console.WriteLine($"La racine {root} i-éme de {number} est : {CalculateNthRoot(number, root)}");

Console.ReadKey();

/// <summary>
/// Calcule la racine n-ième d'un nombre donné.
/// </summary>
/// <param name="number">Le nombre dont on souhaite calculer la racine.</param>
/// <param name="root">L'indice de la racine.</param>
/// <returns>La racine n-ième du nombre donné.</returns>
double CalculateNthRoot(double number, int root)
{
    double x0 = 1.0;
    double x1 = (1.0 / root) * ((root - 1) * x0 + number / Pow(x0, root - 1));

    while (Abs(x1 - x0) >= 0.000001)
    {
        x0 = x1;
        x1 = (1.0 / root) * ((root - 1) * x0 + number / Pow(x0, root - 1));
    }

    return x1;
}

double Abs(double number)
{
    if (number < 0)
        return Abs(-number);
        
    return number;
}

double Pow(double number, int n)
{
    if (n == 0)
        return 1;

    if (n < 0)
        return 1 / Pow(number, -n);

    return number * Pow(number, n - 1);
}
