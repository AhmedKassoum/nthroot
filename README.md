# Calcul de la racine n-iéme d'un reel en utilisant la méthode de Newton

La racine n-iéme d'un réel positif A, notée $\sqrt[n]{A}$, est **la solution réelle positive** de l'equation $X^{n}$ = A.

_Pour cela, nous partirons du principe que nous avons une fonction f qui est strictement monotone sur l'intervalle [a, b]. Supposons également que f(a) et f(b) ont des signes opposés, ce qui signifie que l'équation f(x) = 0 a bien une solution dans l'intervalle [a, b] selon le théorème des valeurs intermédiaires. Notre objectif est donc de déterminer une solution approximative de l'équation f(x) = 0._

**L'objectif est de determiner une solution approché de l'equation f(x) = 0.**

> Rappel : Lorsqu'on a une courbe (Cf), l'équation de la tangente (T) à une courbe (Cf) au point d'abscisse x0 est donné par : y = f'(x0)(x - x0) + f(x0)

Le principal algorithme de calcul de la racine n-ième utilise une suite définie par récurrence pour trouver une valeur approchée de cette racine réelle : 
1. Choisir une valeur approchée initiale x0.
2. Calculer $x_{k+1}$ = $\dfrac{1}{n}[(n-1)$ $x_{k}$ + $\dfrac{A}{x^{n-1}}]$
3. Recommencer à l'étape 2 jusqu'à atteindre la précision voulue.

La demonstration est dans le fichier ci joint [demonstration.pdf](uploads/702a3ecbd03434865d3a5265e3569ed2/demonstration.pdf)

**Algorithme NthRoot**

Variables
- `number` : réel
- `root` : entier

Début
1. `x0` ← 1.0
2. `x1` ← (1.0 / `root`) * ((`root` - 1) * `x0` + `number` / Pow(`x0`, `root` - 1))

Tant que (`Abs(`x1` - `x0`) >= 0.000001) faire
1. `x0` ← `x1`
2. `x1` ← (1.0 / `root`) * ((`root` - 1) * `x0` + `number` / Pow(`x0`, `root` - 1))

**Fin tant que**

**Retourner** x1

**Fin**
